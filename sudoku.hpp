#ifndef SUDOKU_HPP
#define SUDOKU_HPP

#include <vector>
#include <tuple>
#include <algorithm>
#include <thread>
#include <stack>
#include <chrono>
#include <cstdlib>
#include "Box.hpp"

class Sudoku
{
private:
  std::vector<std::vector<Box>> tab;
  unsigned int dimension = 3;
  std::vector<std::tuple<int, int>> initialPositions;
public:
  long iteration = 0;
  std::vector<std::vector<Box>> getTableau() const;
  void setTableau(std::vector<std::vector<Box>>);
  void editTableau(int, int, unsigned int);
  void resetValeur(int, int);
  void incrementTableau(int, int);
  unsigned int getDimension() const;
  unsigned int getElement(int, int) const;
  void setDimentsion(unsigned int);
  Sudoku();
  void afficherSudoku() const;
  bool checkRow(int) const;
  bool checkColumn(int) const;
  bool checkValid(int, int) const;
  void resolve();
  std::vector<std::tuple<int, int>> getInitialPositions() const;
  bool isSolved() const;
};

#endif