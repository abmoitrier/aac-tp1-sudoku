#include "Box.hpp"
#include <random>
#include <string>

/**
 * @brief  Vérifie si la boite est valide
 * 
 * @return true 
 * @return false 
 */
bool Box::checkBox() const {
  bool seen[dimension * dimension + 1] = {false};
  for (int i = 0; i < dimension * dimension; i++) {
    if (box.at(i) != 0) {
      if (seen[box.at(i)]) {
        return false;
      }
      seen[box.at(i)] = true;
    }
  }
  return true;
}

/**
 * @brief Récupère un élément de la boite
 * 
 * @param indice 
 * @return unsigned int 
 */
unsigned int Box::getElement(int indice) const{
  return box.at(indice);
}

/**
 * @brief Récupère un élément de la boite
 * 
 * @param newbox 
 */
void Box::setBox(std::vector<unsigned int> newbox) { box = newbox; }

/**
 * @brief Construit une boite vide
 * 
 */
Box::Box() {
  for (int i = 0; i < dimension * dimension; i++) {
    box.push_back(0);
  }
}

/**
 * @brief Construit une boite avec des éléments aléatoires
 * 
 * @param seed 
 */
Box::Box(long long seed) {
  std::mt19937 mt(seed);
  for (int i = 0; i < dimension * dimension; i++) {
    box.push_back(mt() % (dimension * dimension) + 1);
  }
}

/**
 * @brief  Récupère la boite
 * 
 * @return std::vector<unsigned int> 
 */
std::vector<unsigned int> Box::getBox() const { return box; }

void Box::setElement(int i, int j, unsigned int val) {
  box.at(i * dimension + j) = val;
}

/**
 * @brief Retourne la boite sous forme de liste de string pour l'affichage
 * 
 * @return std::vector<std::string> 
 */
std::vector<std::string> Box::toListString() const {
  std::vector<std::string> retour;
  for (int i = 0; i < dimension; i++) {
    std::string ligne;
    for (int j = 0; j < dimension; j++) {
      ligne += std::to_string(box.at(i * dimension + j));
      ligne += " ";
    }
    retour.push_back(ligne);
  }
  return retour;
}