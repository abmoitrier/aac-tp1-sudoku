#include "main.hpp"
#include "sudoku.hpp"

// manque le resolve

int main()
{
    Sudoku s;
    s.editTableau(0, 0, 5);
    s.editTableau(0, 1, 3);
    s.editTableau(0, 4, 7);
    s.editTableau(1, 0, 6);
    s.editTableau(1, 3, 1);
    s.editTableau(1, 4, 9);
    s.editTableau(1, 5, 5);
    s.editTableau(2, 1, 9);
    s.editTableau(2, 2, 8);
    s.editTableau(2, 7, 6);
    s.editTableau(3, 0, 8);
    s.editTableau(3, 4, 6);
    s.editTableau(3, 8, 3);
    s.editTableau(4, 0, 4);
    s.editTableau(4, 3, 8);
    s.editTableau(4, 5, 3);
    s.editTableau(4, 8, 1);
    s.editTableau(5, 0, 7);
    s.editTableau(5, 4, 2);
    s.editTableau(5, 8, 6);
    s.editTableau(6, 1, 6);
    s.editTableau(6, 6, 2);
    s.editTableau(6, 7, 8);
    s.editTableau(7, 3, 4);
    s.editTableau(7, 4, 1);
    s.editTableau(7, 5, 9);
    s.editTableau(7, 8, 5);
    s.editTableau(8, 4, 8);
    s.editTableau(8, 7, 7);
    s.editTableau(8, 8, 9);

    s.afficherSudoku();
    std::cout << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    s.resolve();
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = end - start;

    s.afficherSudoku();
    if(!s.isSolved()){
        std::cout << "Le sudoku n'a pas de solution\n";
    } 
    std::cout << "\n"
              << s.iteration << "\n";
    std::cout << "Sudoku solved in " << diff.count() << " seconds\n";
    return 0;
}