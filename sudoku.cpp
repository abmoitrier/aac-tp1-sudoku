#include "sudoku.hpp"
#include <random>

/**
 * @brief Modifie la valeur d'une case du tableau
 *
 * @param i ligne 
 * @param j colonne
 * @param val valeur
 */
void Sudoku::editTableau(int i, int j, unsigned int val)
{
  int ligne = i / dimension;
  int column = j / dimension;
  int indicei = i;
  int indicej = j;
  while (indicei >= dimension)
  {
    indicei -= dimension;
  }
  while (indicej >= dimension)
  {
    indicej -= dimension;
  }
  tab.at(ligne).at(column).setElement(indicei, indicej, val);
  initialPositions.push_back(std::make_tuple(i, j));
}

/**
 * @brief Incrémente la valeur d'une case du tableau
 *
 * @param i ligne 
 * @param j colonne
 */
void Sudoku::incrementTableau(int i, int j)
{
  int ligne = i / dimension;
  int column = j / dimension;
  int indicei = i;
  int indicej = j;
  while (indicei >= dimension)
  {
    indicei -= dimension;
  }
  while (indicej >= dimension)
  {
    indicej -= dimension;
  }
  tab.at(ligne).at(column).setElement(
      indicei, indicej,
      tab.at(ligne).at(column).getElement(indicei * dimension + indicej) + 1);
}

/**
 * @brief Récupère le tableau
*/
std::vector<std::vector<Box>> Sudoku::getTableau() const { return tab; }

/**
 * @brief Modifie le tableau
*/
void Sudoku::setTableau(std::vector<std::vector<Box>> newtab) { tab = newtab; }
/**
 * @brief Récupère la dimension
*/
unsigned int Sudoku::getDimension() const { return dimension; }
/**
 * @brief Modifie la dimension
*/
void Sudoku::setDimentsion(unsigned int newdim) { dimension = newdim; }

/**
 * @brief Construit un sudoku vide
*/
Sudoku::Sudoku()
{
  // std::mt19937 mt(4564865456456);
  // for (int k = 0; k < dimension; k++) {
  //   std::vector<Box> ligne;
  //   for (int i = 0; i < dimension; i++) {
  //     ligne.push_back(Box(mt()));
  //   }
  //   tab.push_back(ligne);
  // }
  for (int k = 0; k < dimension; k++)
  {
    std::vector<Box> ligne;
    for (int i = 0; i < dimension; i++)
    {
      ligne.push_back(Box());
    }
    tab.push_back(ligne);
  }
}

void Sudoku::afficherSudoku() const
{
  for (std::vector<Box> ligne : tab)
  {
    std::vector<std::string> liste;
    std::vector<std::string> result;
    for (int k = 0; k < dimension; k++)
    {
      liste.push_back("");
      result.push_back("");
    }
    for (Box box : ligne)
    {
      std::vector<std::string> listbox = box.toListString();
      for (int i = 0; i < dimension; ++i)
      {
        result[i] = liste[i] + listbox[i];
        liste[i] = result[i];
      }
    }
    for (int i = 0; i < dimension; i++)
    {
      std::cout << result[i] << std::endl;
    }
  }
}

/**
 * @brief Vérifie si une ligne est valide
*/
bool Sudoku::checkRow(int ligne) const
{
  std::vector<bool> seen(dimension * dimension + 1, false);
  for (int j = 0; j < dimension * dimension; j++)
  {
    unsigned int val = getElement(ligne, j);
    if (val != 0)
    {
      if (seen[val])
      {
        return false;
      }
      seen[val] = true;
    }
  }
  return true;
}

/**
 * @brief Vérifie si une colonne est valide
 * 
 * @param column 
 * @return true 
 * @return false 
 */
bool Sudoku::checkColumn(int column) const
{
  std::vector<bool> seen(dimension * dimension + 1, false);
  for (int i = 0; i < dimension * dimension; i++)
  {
    unsigned int val = getElement(i, column);
    if (val != 0)
    {
      if (seen[val])
      {
        return false;
      }
      seen[val] = true;
    }
  }
  return true;
}

/**
 * @brief Vérifie si une case est valide
 * 
 * @param i 
 * @param j 
 * @return true 
 * @return false 
 */
bool Sudoku::checkValid(int i, int j) const
{
  return checkRow(i) && checkColumn(j) && tab[i / dimension][j / dimension].checkBox();
}
// bool Sudoku::checkValid(int i, int j) const
// {
//   int ligne = i / dimension;
//   int column = j / dimension;
//   Box box = tab.at(ligne).at(column);
//   return (checkColumn(j) && checkRow(i) && box.checkBox());
// }

/**
 * @brief Réinitialise la valeur d'une case
 * 
 * @param i 
 * @param j 
 */
void Sudoku::resetValeur(int i, int j)
{
  tab[i / dimension][j / dimension].setElement(i % dimension, j % dimension, 0);
}

/**
 * @brief Récupère la valeur d'une case
 * 
 * @param i 
 * @param j 
 * @return unsigned int 
 */
unsigned int Sudoku::getElement(int i, int j) const
{
  return tab[i / dimension][j / dimension].getElement(i % dimension * dimension + j % dimension);
}

/**
 * @brief Récupère les positions initiales
 * 
 * @return std::vector<std::tuple<int, int>> 
 */
std::vector<std::tuple<int, int>> Sudoku::getInitialPositions() const
{
  return initialPositions;
}

/**
 * @brief Vérifie si le sudoku est résolu
 * 
 * @return true 
 * @return false 
 */
bool Sudoku::isSolved() const
{
  for (int i = 0; i < dimension * dimension; ++i)
  {
    for (int j = 0; j < dimension * dimension; ++j)
    {
      if (getElement(i, j) == 0 || !checkValid(i, j))
      {
        return false;
      }
    }
  }
  return true;
}

/**
 * @brief Résoud le sudoku (j'aurai du simplifier cette fonction en utilisant une fonction récursive mais je n'y ai pas pensé au début et je n'ai pas eu le temps de le faire après)
 * 
 */
void Sudoku::resolve()
{
  int i = 0;
  int j = 0;
  while (++iteration) // il s'agit d'une while true qui sert en même temps à incrémenter l'itération pour savoir où j'en suis
  {

    // if(iteration % 1000 == 0){ // on affiche le sudoku tous les 1000 itérations
    //   system("clear");
    //   afficherSudoku();
    //   std::cout << iteration <<"\n";
    // }
    while (std::find_if(initialPositions.begin(), initialPositions.end(), // on vérifie que la case n'est pas une case initiale
                        [&](const std::tuple<int, int> &t)
                        {
                          return std::get<0>(t) == i && std::get<1>(t) == j;
                        }) != initialPositions.end())
    {
      if (j < dimension * dimension - 1) // on incrémente j si on peut
      {

        j++;
      }
      else // sinon on incrémente i et on remet j à 0
      {
        if (i < dimension * dimension - 1)
        {
          i++;
          j = 0;
        }
        else  // sinon on a fini si on ne peut pas incrémenter i
        {
          return;
        }
      }
    }
    if (getElement(i, j) < dimension * dimension) // si la valeur de la case est inférieure à la dimension au carré on l'incrémente
    {
      incrementTableau(i, j);
    }
    else // sinon on réinitialise la valeur de la case et on décrémente j si on peut
    {
      resetValeur(i, j);
      if (j > 0)
      {
        j--;
      }
      else // sinon on décrémente i et on remet j à dimension * dimension - 1
      {
        if (i > 0)
        {
          i--;
          j = dimension * dimension - 1;
        }
        else // sinon on a fini si on ne peut pas décrémenter i
        {
          return;
        }
      }

      while (std::find_if(initialPositions.begin(), initialPositions.end(),  // on vérifie que la case n'est pas une case initiale
                          [&](const std::tuple<int, int> &t)
                          {
                            return std::get<0>(t) == i && std::get<1>(t) == j;
                          }) != initialPositions.end())
      {
        if (j > 0) // on décrémente j si on peut
        {
          j--;
        }
        else // sinon on décrémente i et on remet j à dimension * dimension - 1
        {
          if (i > 0)
          {
            i--;
            j = dimension * dimension - 1;
          }
          else // sinon on a fini si on ne peut pas décrémenter i
          {
            return;
          }
        }
      }
      continue; // on continue la boucle si on a décrémenté i ou j
    }

    while (!checkValid(i, j) && getElement(i, j) < dimension * dimension) // on incrémente la valeur de la case tant qu'elle n'est pas valide
    {
      incrementTableau(i, j);
    }

    if (getElement(i, j) == dimension * dimension && !checkValid(i, j)) // si la valeur de la case est égale à la dimension au carré et qu'elle n'est pas valide on réinitialise la valeur de la case
    {
      resetValeur(i, j);
      if (j > 0) // on décrémente j si on peut
      {
        j--;
      }
      else // sinon on décrémente i et on remet j à dimension * dimension - 1 
      {
        if (i > 0)
        {
          i--;
          j = dimension * dimension - 1;
        }
        else // sinon on a fini si on ne peut pas décrémenter i
        {
          return;
        }
      }
      while (std::find_if(initialPositions.begin(), initialPositions.end(), // on vérifie que la case n'est pas une case initiale
                          [&](const std::tuple<int, int> &t)
                          {
                            return std::get<0>(t) == i && std::get<1>(t) == j;
                          }) != initialPositions.end())
      {
        if (j > 0) // on décrémente j si on peut
        {
          j--;
        }
        else // sinon on décrémente i et on remet j à dimension * dimension - 1
        {
          if (i > 0)
          {
            i--;
            j = dimension * dimension - 1;
          }
          else // sinon on a fini si on ne peut pas décrémenter i
          {
            return;
          }
        }
      }
    }
    else // sinon on incrémente j si on peut
    {
      if (j < dimension * dimension - 1)
      {
        j++;
      }
      else // sinon on incrémente i et on remet j à 0
      {
        if (i < dimension * dimension - 1)
        {
          i++;
          j = 0;
        }
        else // sinon on a fini si on ne peut pas incrémenter i
        {
          return;
        }
      }
    }
  }
}