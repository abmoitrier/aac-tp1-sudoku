#ifndef BOX_HPP
#define BOX_HPP

#include <vector>
#include <random>
#include <iostream>
#include <string>

class Box{
    private:
      std::vector<unsigned int> box;
      unsigned int dimension = 3;
    public:
      std::vector<unsigned int> getBox() const;
      void setBox(std::vector<unsigned int>);
      bool checkBox() const;
      Box();
      void setElement(int, int, unsigned int);
      unsigned int getElement(int) const;
      std::vector<std::string> toListString() const;
      Box(long long);
};

#endif